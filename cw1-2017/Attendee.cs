﻿/*
 * Author: Marco Moroni
 * Description: It stores the attendee info
 * Date last modified: 2016/10/17
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cw1_2017
{

    class Attendee : Person
    {

        public enum RegType { Full, Student, Organiser };



        //// PROPERTIES ////

        private int attendeeRef;
        private string conferenceName;
        private RegType registrationType;
        private bool paid;
        private bool presenter;
        private string paperTitle;
        private bool isComplete = false;                // The object has all valid properties

        private int minAttendeeRefNo = 40000;
        private int maxAttendeeRefNo = 60000;



        //// CONSTRUCTORS ////

        public Attendee() : base ()
        {

        }



        //// ACCESSORS ////

        // Get/set attendee reference number
        public int AttendeeRef
        {
            get { return attendeeRef; }
            set
            {
                if (value < minAttendeeRefNo || value > maxAttendeeRefNo)
                {
                    throw new ArgumentException("The refrence number must be between 4000 and 60000.");
                }
                attendeeRef = value;
            }
        }

        // Get/set conference name
        public string ConferenceName
        {
            get { return conferenceName; }
            set
            {
                if (String.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException("A conference name is needed.");
                }
                conferenceName = value;
            }
        }

        // Get/set registration type
        public RegType RegistrationType
        {
            get { return registrationType; }
            set
            {
                if (!Enum.IsDefined(typeof(RegType), value))
                {
                    throw new ArgumentException("Registration type is wrong.");
                }
                registrationType = value;
            }
        }

        // Get/set paid
        public bool Paid
        {
            get { return paid; }
            set { paid = value; }
        }

        // Get presenter
        public bool Presenter
        {
            get { return presenter; }
        }

        // Get paper title
        public string PaperTitle
        {
            get { return paperTitle; }
        }

        // Get/set is complete
        public bool IsComplete
        {
            get { return isComplete; }
            set { isComplete = value; }
        }



        //// METHODS ////

        // Set the paper title with the presenter
        public void setPaperTitleAndPresenter(bool presenter, string paperTitle)
        {
            // If presenter is true and there is a paper title set both fields
            if(presenter && !String.IsNullOrWhiteSpace(paperTitle))
            {
                this.presenter = presenter;
                this.paperTitle = paperTitle;
            }
            // if presenter is false then there is no paper title
            else if (!presenter)
            {
                this.presenter = presenter;
                this.paperTitle = String.Empty;
            }
            // if the presenter is true but there is no paper title throw an exception
            else
            {
                throw new ArgumentException("A paper title is required");
            }
        }

        // Get the cost of the event
        public double getCost()
        {

            double fullCost = 500;
            double studentCost = 300;
            double organiserCost = 0;

            // Calculate discount
            double discount = 0;
            if (presenter) discount = 0.1;

            // Return cost with discount
            switch(RegistrationType)
            {
                case(RegType.Full):
                    return fullCost - fullCost * discount;
                case(RegType.Student):
                    return studentCost - studentCost * discount;
                case(RegType.Organiser):
                    return organiserCost - organiserCost * discount;
                default:
                    throw new ArgumentException("Registration type is not valid.");
            }
        }

    }
}
