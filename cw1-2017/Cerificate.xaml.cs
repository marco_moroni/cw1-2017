﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace cw1_2017
{
    /// <summary>
    /// Interaction logic for Cerificate.xaml
    /// </summary>
    public partial class Cerificate : Window
    {
        public Cerificate()
        {
            InitializeComponent();
        }

        // Without a paper title
        public Cerificate(string firstName, string lastName, string conferenceName) : this()
        {
            // Create the certificate text
            string certificateText = "This is to certify that " + firstName + " " +
                lastName + " attended " + conferenceName + ".";

            // Show the certificate text
            lblCertificate.Content = certificateText;
        }

        // With a paper title
        public Cerificate(string firstName, string lastName, string conferenceName, string paperTitle) : this()
        {
            // Create the certificate text
            string certificateText = "This is to certify that " + firstName + " " +
                lastName + " attended " + conferenceName + " and presented a paper entitled "
                + paperTitle + ".";

            // Show the certificate text
            lblCertificate.Content = certificateText;
        }

    }
}
