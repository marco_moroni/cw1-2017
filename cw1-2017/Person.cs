﻿/*
 * Author: Marco Moroni
 * Description: Person class with just name and institution info
 * (I decided to put institution here because it's not a property of
 * an attendee only)
 * Date last modified: 2016/10/17
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cw1_2017
{
    class Person
    {



        //// PROPERTIES ////

        private string firstName;
        private string lastName;
        private Institution institution;



        //// CONSTRUCTORS ////

        public Person()
        {
            institution = new Institution();
        }



        //// ACCESSORS ////

        // Get/set first name
        public string FirstName
        {
            get { return firstName; }
            set
            {
                if (String.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException("A first name is needed.");
                }
                firstName = value;
            }
        }

        // Get/set last name
        public string LastName
        {
            get { return lastName; }
            set
            {
                if (String.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException("A last name is needed.");
                }
                lastName = value;
            }
        }

        // Get/set institution
        public Institution GetSetInstitution
        {
            get { return institution; }
            set { institution = value; }
        }



    }
}
