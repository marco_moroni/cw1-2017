﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace cw1_2017
{
    /// <summary>
    /// Interaction logic for Invoice.xaml
    /// </summary>
    public partial class Invoice : Window
    {
        public Invoice(string firstName, string lastName, string institution, double cost, bool paid)
        {
            InitializeComponent();

            // Write invoice info
            string completeName = firstName + " " + lastName;
            lblInvoiceAttendeeCompleteName.Content = completeName;
            lblInvoiceAttendeeInstitutionName.Content = institution;
            lblInvoiceAttendeeCost.Content = "£" + (paid ? "0" : cost.ToString());
        }
    }
}
