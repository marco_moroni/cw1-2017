﻿/*
 * Author: Marco Moroni
 * Description: It stores the institution info
 * Date last modified: 2016/10/17
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cw1_2017
{
    class Institution
    {



        //// PROPERTIES ////

        private string institutionName;
        private string institutionAddress;



        //// CONSTRUCTORS ////

        public Institution()
        {

        }



        //// ACCESSORS ////

        // Get/set institution name
        public string InstitutionName
        {
            get { return institutionName;  }
            set { institutionName = value;  }
        }

        // Get/set institution address
        public string InstitutionAddress
        {
            get { return institutionAddress; }
            set
            {
                // Return exception if there is no institution name
                if (String.IsNullOrWhiteSpace(institutionName) && !String.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException("An institution name is needed.");
                }

                institutionAddress = value;
            }
        }



    }
}
