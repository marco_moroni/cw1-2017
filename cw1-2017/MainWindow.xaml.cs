﻿/*
 * Author: Marco Moroni
 * Description: Code for buttons
 * Date last modified: 2016/10/17
 * */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace cw1_2017
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        Attendee attendee = new Attendee();

        public MainWindow()
        {
            InitializeComponent();
        }

        // Click the "Clear" button to clear all the fields
        private void btnClear_Click(object sender, RoutedEventArgs e)
        {

            // Clear every textbox
            txtbxFirstName.Text = String.Empty;
            txtbxLastName.Text = String.Empty;
            txtbxAttendeeRef.Text = String.Empty;
            txtbxInstitutionAddress.Text = String.Empty;
            txtbxInstitutionName.Text = String.Empty;
            txtbxConferenceName.Text = String.Empty;
            rdobtnRegTypeFull.IsChecked = false;
            rdobtnRegTypeStudent.IsChecked = false;
            rdobtnRegTypeOrganiser.IsChecked = false;
            txtbxPaperTitle.Text = String.Empty;

            // Uncheck every checkbox
            checkbxPaid.IsChecked = false;
            checkbxPresenter.IsChecked = false;

            // ? registration type ?

        }

        // Click the "Get" button to write the attendee info into the fields
        private void btnGet_Click(object sender, RoutedEventArgs e)
        {

            // Check if attendee is valid
            if (attendee.IsComplete)
            {
                txtbxFirstName.Text = attendee.FirstName;
                txtbxLastName.Text = attendee.LastName;
                txtbxAttendeeRef.Text = attendee.AttendeeRef.ToString();
                txtbxInstitutionName.Text = attendee.GetSetInstitution.InstitutionName;
                txtbxInstitutionAddress.Text = attendee.GetSetInstitution.InstitutionAddress;
                txtbxConferenceName.Text = attendee.ConferenceName;

                // Registration type
                switch (attendee.RegistrationType)
                {
                    case Attendee.RegType.Full:
                        rdobtnRegTypeFull.IsChecked = true;
                        break;
                    case Attendee.RegType.Student:
                        rdobtnRegTypeStudent.IsChecked = true;
                        break;
                    case Attendee.RegType.Organiser:
                        rdobtnRegTypeOrganiser.IsChecked = true;
                        break;
                }

                checkbxPaid.IsChecked = attendee.Paid;
                checkbxPresenter.IsChecked = attendee.Presenter;
                txtbxPaperTitle.Text = attendee.PaperTitle;
            }
            else
            {
                MessageBox.Show("You don't have a complete attendee yet.");
            }

        }

        // Click the "Set" button to read from all the fields
        private void btnSet_Click(object sender, RoutedEventArgs e)
        {

            try
            {

                // Try to set everything
                attendee.FirstName = txtbxFirstName.Text;
                attendee.LastName = txtbxLastName.Text;
                if (txtbxAttendeeRef.Text.All(char.IsDigit))
                {
                    // Set the attendee ref only if there only are numbers in the text box
                    // (and set a 0 if the text box is empty)
                   attendee.AttendeeRef = string.IsNullOrWhiteSpace(txtbxAttendeeRef.Text) ? 0 : Int32.Parse(txtbxAttendeeRef.Text);
                }
                else
                {
                    throw new ArgumentException("Only number allowed in Attendee Ref.");
                }
                attendee.GetSetInstitution.InstitutionName = txtbxInstitutionName.Text;
                attendee.GetSetInstitution.InstitutionAddress = txtbxInstitutionAddress.Text;
                attendee.ConferenceName = txtbxConferenceName.Text;
                if (rdobtnRegTypeFull.IsChecked.Value || rdobtnRegTypeStudent.IsChecked.Value || rdobtnRegTypeOrganiser.IsChecked.Value)
                {
                    // Set registration type only if there is a selection, otherwise throw an error
                    if (rdobtnRegTypeFull.IsChecked.Value)
                        attendee.RegistrationType = Attendee.RegType.Full;
                    else if (rdobtnRegTypeStudent.IsChecked.Value)
                        attendee.RegistrationType = Attendee.RegType.Student;
                    else if (rdobtnRegTypeOrganiser.IsChecked.Value)
                        attendee.RegistrationType = Attendee.RegType.Organiser;
                }
                else
                {
                    throw new ArgumentException("Please select your registration type.");
                }
                attendee.Paid = checkbxPaid.IsChecked.Value;
                attendee.setPaperTitleAndPresenter(checkbxPresenter.IsChecked.Value, txtbxPaperTitle.Text);

                // If there are no errors it means the attendee is complete
                attendee.IsComplete = true;
            }
            catch (ArgumentException exep)
            {
                MessageBox.Show(exep.Message);
            }

        }

        // click "Invoice" to open a new invoice window
        private void btnInvoice_Click(object sender, RoutedEventArgs e)
        {
            // Check if attendee is valid
            if (attendee.IsComplete)
            {
                // Show the invoice window
                Invoice invoiceWindow = new Invoice(attendee.FirstName, attendee.LastName, attendee.GetSetInstitution.InstitutionName, attendee.getCost(), attendee.Paid);
                invoiceWindow.Show();
            }
            else
            {
                MessageBox.Show("You don't have a complete attendee yet.");
            }
        }

        // click "Certificate" to open a new invoice window
        private void btnCertificate_Click(object sender, RoutedEventArgs e)
        {
            // Check if attendee is valid
            if (attendee.IsComplete)
            {
                // Show the certificate window
                // Use a defferent Certificate constructor if the attendee is the presenter
                if (!attendee.Presenter)
                {
                    Cerificate certificateWindow = new Cerificate(attendee.FirstName, attendee.LastName, attendee.ConferenceName);
                    certificateWindow.Show();
                }
                else
                {
                    Cerificate certificateWindow = new Cerificate(attendee.FirstName, attendee.LastName, attendee.ConferenceName, attendee.PaperTitle);
                    certificateWindow.Show();
                }
            }
            else
            {
                MessageBox.Show("You don't have a complete attendee yet.");
            }
        }
    }
}
